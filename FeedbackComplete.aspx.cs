﻿using System;

/// <summary>
/// code-behind for the completed feedback page
/// </summary>
/// <author>
/// Justin McConnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class FeedbackComplete : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ContactYes"] != null)
        {
            if ((Boolean)Session["ContactYes"])
            {
                this.lblMessage.Text = this.lblMessage.Text + "Someone will be contacting you shortly.";
            }
        }
    }
    /// <summary>
    /// Handles the Click event of the btnReturn control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerFeedback.aspx");
    }
}