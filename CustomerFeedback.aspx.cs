﻿using System;

/// <summary>
/// the feedback page
/// </summary>
/// <author>
/// Justin Mcconnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class CustomerFeedback : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCustomerID_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            SetFocus(this.lstClosedFeedback);
            this.SetButtonsEnabled(true);
        }
        else
        {   
            this.SetButtonsEnabled(false);
        }
        
    }
    /// <summary>
    /// Handles the Click event of the btnSubmitAll control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSubmitAll_Click(object sender, EventArgs e)
    {
        if (this.chbxContacted.Checked)
        {
            Session["ContactYes"] = true;
        }
        Response.Redirect("FeedbackComplete.aspx");
    }

    /// <summary>
    /// Sets the buttons enabled.
    /// </summary>
    /// <param name="enabled">if set to <c>true</c> [enabled].</param>
    public void SetButtonsEnabled(Boolean enabled)
    {
        this.rbnServiceSatisfied.Enabled = enabled;
        this.rbnServiceNeither.Enabled = enabled;
        this.rbnServiceDissatisfied.Enabled = enabled;

        this.rbnEfficiencySatisfied.Enabled = enabled;
        this.rbnEfficiencyNeither.Enabled = enabled;
        this.rbnEfficiencyDissatisfied.Enabled = enabled;

        this.rbnResolutionSatisfied.Enabled = enabled;
        this.rbnResolutionNeither.Enabled = enabled;
        this.rbnResolutionDissatisfied.Enabled = enabled;

        this.txtComments.Enabled = enabled;
        this.chbxContacted.Enabled = enabled;
        this.rbnEmail.Enabled = enabled;
        this.rbnPhone.Enabled = enabled;
        this.btnSubmitAll.Enabled = enabled;

    }
    /// <summary>
    /// Handles the SelectedIndexChanged event of the lstClosedFeedback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lstClosedFeedback_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.lstClosedFeedback.SelectedIndex == -1)
        {
            this.SetButtonsEnabled(false);
        }
        else
        {
            this.SetButtonsEnabled(true);
        }
    }
}