﻿using System;

/// <summary>
/// master file
/// </summary>
/// <author>
/// Justin Mcconnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class Site : System.Web.UI.MasterPage
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
