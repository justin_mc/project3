﻿<%@ Page Title="Software Incidents" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="IncidentDisplay.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Please select a customer to view their reported incidents:</h2>
    <asp:DropDownList ID="ddlCustomerList" runat="server" DataSourceID="sdsCustomerList" DataTextField="Name" DataValueField="Name" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" AutoPostBack="True" Height="31px" Width="290px">
    </asp:DropDownList>
    <asp:SqlDataSource ID="sdsCustomerList" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerConnect %>" ProviderName="<%$ ConnectionStrings:CustomerConnect.ProviderName %>" SelectCommand="SELECT [Name] FROM [Customer]"></asp:SqlDataSource>
    <br />
    <br />
    <asp:DataList ID="dtlIncidentDetails" runat="server" CellPadding="4" DataSourceID="sdsIncidents" RepeatColumns="1" RepeatLayout="Flow" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" ForeColor="#333333" RepeatDirection="Horizontal">
        <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
        <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
        <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <ItemTemplate>
            <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
            &nbsp;<asp:Label ID="Software__IncidentLabel" runat="server" Text='<%# Eval("[Software/ Incident]") %>' />
            &nbsp;<asp:Label ID="Technician_NameLabel" runat="server" Text='<%# Eval("[Technician Name]") %>' />
            &nbsp;<asp:Label ID="Date_ClosedLabel" runat="server" Text='<%# Eval("[Date Closed]") %>' />
            <br />
            <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>' />
            <br />
        </ItemTemplate>
        <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:DataList>
    <asp:SqlDataSource ID="sdsIncidents" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerConnect %>" ProviderName="<%$ ConnectionStrings:CustomerConnect.ProviderName %>" SelectCommand="SELECT DISTINCT Customer.Name, Software.Name AS [Software/ Incident], Support.Name AS [Technician Name], Feedback.DateOpened AS [Date Opened], Feedback.DateClosed AS [Date Closed], Feedback.Description FROM Support, Feedback, Software, Customer WHERE ([Customer.Name] = ?)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlCustomerList" Name="?" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>

