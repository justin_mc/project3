﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="FeedbackComplete.aspx.cs" Inherits="FeedbackComplete" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <section>
        <article>
            <p>
                <asp:Label ID="lblMessage" runat="server" Text="Thank you for your feedback. " CssClass="message"></asp:Label>
                <br/>
                <br/>
                <asp:Button ID="btnReturn" runat="server" OnClick="btnReturn_Click" Text="Return to Feedback Page" CssClass="button" />
            </p>
        </article>
    </section>
</asp:Content>
